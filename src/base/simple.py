import torch
import torch.nn as nn

from src.torch_utils.layers import Conv2d


class BaseNetwork(nn.Module):
    def __init__(self):
        super().__init__()
        self.feature_extractor = nn.Sequential(
            Conv2d(3, 16, kernel_size=5, stride=2, padding=2),
            Conv2d(16, 32, stride=2, padding=1),
            Conv2d(32, 64, stride=2, padding=1),
            Conv2d(64, 128, stride=2, padding=1),
            Conv2d(128, 256, stride=2, padding=1))
        # self.conv6 = Conv2d(64, 64, stride=2, padding=1)

    def forward(self, input_data: torch.Tensor) -> torch.Tensor:
        return self.feature_extractor(input_data)
