import colorsys
import math
from typing import List, Tuple

import numpy as np
import torch
import torch.nn as nn

from config.ssd_config import SSDConfig
from src.box import check_rectangle
from src.torch_utils.layers import Conv2d


class SSD(nn.Module):

    class Layer(nn.Module):
        def __init__(self, input_features: int, output_features: int, detector_dim: int, **kwargs):
            super(SSD.Layer, self).__init__()
            self.detector_dim = detector_dim
            self.detector = Conv2d(input_features, detector_dim, kernel_size=3, padding=1,
                                   batch_norm=False, activation=None)
            self.conv = Conv2d(
                input_features, output_features, **kwargs) if 'disable' not in kwargs else None

        def forward(self, input_data: torch.Tensor) -> torch.Tensor:
            return (
                self.conv(input_data) if self.conv is not None else None,
                self.detector(input_data).permute(0, 2, 3, 1))

    class AnchorInfo:
        def __init__(self, center_y: float, center_x: float, height: float, width: float,
                     index: int, layer_index: int, map_index: Tuple[int, int], color_index: int,
                     ratio: float, size_factor: float):
            self.index = index
            self.layer_index = layer_index
            self.map_index = map_index
            self.color_index = color_index
            self.ratio = ratio
            self.size_factor = size_factor
            self.center_y = center_y
            self.center_x = center_x
            self.height = height
            self.width = width
            self.y_min, self.x_min, self.y_max, self.x_max = check_rectangle(
                center_y - (height / 2), center_x - (width / 2), center_y + (height / 2), center_x + (width / 2))

        def __repr__(self):
            return (f'{self.__class__.__name__}'
                    f'(index:{self.index}, layer:{self.layer_index}, coord:{self.map_index}'
                    f', center:({self.center_y:.03f}, {self.center_x:.03f})'
                    f', size:({self.height:.03f}, {self.width:.03f})'
                    f', ratio:{self.ratio:.03f}, size_factor:{self.size_factor:.03f})'
                    f', y:[{self.y_min:.03f}:{self.y_max:.03f}]'
                    f', x:[{self.x_min:.03f}:{self.x_max:.03f}])')

        def __array__(self):
            return np.array([self.center_y, self.center_x, self.height, self.width])

    def __init__(self, base_network: nn.Module, input_sample: np.ndarray):
        super(SSD, self).__init__()

        self.location_dim = SSDConfig.LOCATION_DIMMENSION
        self.classes = SSDConfig.OUTPUT_CLASSES + 1
        self.base_input_shape = input_sample.shape[1:]
        sample = torch.as_tensor(input_sample, dtype=torch.float32)
        self.base_network = base_network
        sample_output = base_network(sample)
        self.base_output_shape = list(sample_output.shape)[-3:]

        layers: List[SSD.Layer] = []
        last_feature_count = self.base_output_shape[0]
        for layer_index, (output_features, kwargs) in enumerate(zip(SSDConfig.LAYER_FEATURES, SSDConfig.LAYER_ARGS)):
            layers.append(SSD.Layer(
                last_feature_count, output_features,
                (self.classes + self.location_dim) * len(SSDConfig.LAYER_RATIOS[layer_index]),
                **kwargs))
            last_feature_count = output_features
        self.layers = nn.ModuleList(layers)

        self.anchors_numpy, self.anchor_info, self.box_colors = self._create_anchors(sample_output, self.layers)
        self.anchors = torch.tensor(self.anchors_numpy, dtype=torch.float32, requires_grad=False)

    def forward(self, input_data: torch.Tensor) -> torch.Tensor:
        base_output = self.base_network(input_data)
        head = base_output
        outputs = []
        for layer in self.layers:
            head, detector_output = layer(head)
            outputs.append(detector_output.reshape(base_output.size(0), -1, self.classes + self.location_dim))
        outputs = torch.cat(outputs, 1)
        return torch.cat(
            [outputs[:, :, :self.location_dim], torch.softmax(outputs[:, :, self.location_dim:], dim=2)], dim=2)

    def _apply(self, fn):
        super()._apply(fn)
        self.anchors = fn(self.anchors)
        return self

    @staticmethod
    def _create_anchors(base_output: torch.Tensor, layers: nn.ModuleList) -> Tuple[
            np.ndarray, np.ndarray, List[np.ndarray]]:
        anchors = []
        anchor_info: List[SSD.AnchorInfo] = []
        box_colors: List[np.ndarray] = []
        head = base_output

        # layer_count = 0
        # for layer_index in range(len(layers)):
        #     layer_count += len(SSDConfig.LAYER_RATIOS[layer_index])

        for layer_index, layer in enumerate(layers):
            head, detector_output = layer(head)  # detector output shape : NCRSHW (Ratio, Size)

            detector_rows = detector_output.size()[1]
            detector_cols = detector_output.size()[2]
            color_index = 0
            layer_ratios = SSDConfig.LAYER_RATIOS[layer_index]
            for index_y in range(detector_rows):
                center_y = (index_y + 0.5) / detector_rows
                for index_x in range(detector_cols):
                    center_x = (index_x + 0.5) / detector_cols
                    for ratio, size_factor in zip(layer_ratios, SSDConfig.BOX_SIZE_FACTORS):
                        box_colors.append((np.asarray(colorsys.hsv_to_rgb(
                            color_index / len(layer_ratios), 1.0, 1.0)) * 255).astype(np.uint8))
                        color_index += 1
                        unit_box_size = size_factor / max(detector_rows, detector_cols)
                        if ratio != 1:
                            anchor_width = unit_box_size * math.sqrt(ratio)
                            anchor_height = unit_box_size / math.sqrt(ratio)
                        else:
                            anchor_width = unit_box_size
                            anchor_height = unit_box_size
                        anchor_info.append(SSD.AnchorInfo(
                            center_y, center_x,
                            anchor_height, anchor_width,
                            len(anchors),
                            layer_index,
                            (index_y, index_x),
                            len(box_colors) - 1,
                            ratio,
                            size_factor
                        ))
                        anchors.append([center_y, center_x, anchor_height, anchor_width])
        return np.asarray(anchors), anchor_info, box_colors
