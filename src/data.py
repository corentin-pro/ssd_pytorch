import os
from typing import Iterable, Optional, Tuple

import h5py
import numpy as np
from PIL import Image

from src.torch_utils.utils.logger import DummyLogger


def load_data(
        dataset: str,
        path: str,
        return_images: bool = False,
        class_list: Optional[Iterable[str]] = None,
        limit: Optional[int] = None,
        size: Optional[Iterable[int]] = None,
        save: Optional[str] = None,
        logger=DummyLogger) -> Tuple[np.ndarray, np.ndarray]:
    data_loaded = False
    if save:
        save_file_name = save + '.hdf5'
        logger.info('Looking for existing save file : {}'.format(save_file_name))
        if os.path.exists(save_file_name):
            logger.info('Loading data from files {}'.format(save_file_name))
            with h5py.File(save_file_name, 'r') as h5_file:
                train_data = np.asarray(h5_file['data'])
                train_labels = np.asarray(h5_file['label'])
            data_loaded = True
        else:
            logger.info('Save file not found')
    if not data_loaded:
        if not path:
            logger.error('--path is needed if save file is not set nor found')
            exit(1)
        if dataset == 'pascal_voc_2012':
            from src.dataset.pascal_voc_2012 import PascalVoc2012
            logger.info('Loading labels from PascalVoc2012 folder : {}'.format(path))
            train_data, train_labels = PascalVoc2012.load_data(
                path, return_images=return_images, class_list=class_list, data_limit=limit,
                resize=size, logger=logger)
            if return_images:
                train_data = np.transpose(train_data, (0, 3, 1, 2))

            if save:
                logger.info('Writing data in file : {}'.format(save + '.hdf5'))
                os.makedirs(os.path.dirname(save), exist_ok=True)
                with h5py.File(save_file_name, 'w') as h5_file:
                    h5_file.create_dataset('data', data=train_data)
                    h5_file.create_dataset('label', data=train_labels)
        else:
            logger.error('Dataset not implemented')
            exit(1)
    return train_data, train_labels


def resize_image(image: Image.Image, resize: Tuple[int, int]) -> Image.Image:
    image.thumbnail(resize)
    resized_image = Image.new('RGB', resize, (0, 0, 0,))
    resized_image.paste(
        image,
        ((resize[0] - image.size[0]) // 2, (resize[1] - image.size[1]) // 2))
    return resized_image
