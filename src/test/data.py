import numpy as np
from torch.utils.tensorboard import SummaryWriter


def test(data: np.ndarray, labels: np.ndarray, writer: SummaryWriter, global_step: int = 0):
    """
    images are in format [NCHW]
    label are sequences of boxes (box : y, x, h, w, one hot for class)
    """

    # Tensorboard test : outputing annotated images
    box_color = np.array([255, 0, 0], dtype=np.uint8)
    for image_index, raw_image in enumerate(data):
        # writer.add_image(f'input/image_{image_index}', raw_image / 255)
        image = np.array(raw_image, copy=True)
        image = np.transpose(image, (1, 2, 0))  # CHW -> HWC
        label = labels[image_index]
        for box in label:
            if box[3] == 0:
                break
            center_y = box[0] * image.shape[0]
            center_x = box[1] * image.shape[1]
            halh_height = (box[2] * image.shape[0]) / 2
            halh_width = (box[3] * image.shape[1]) / 2
            y_min = int(round(center_y - halh_height))
            y_max = int(round(center_y + halh_height))
            x_min = int(round(center_x - halh_width))
            x_max = int(round(center_x + halh_width))
            image[y_min:y_max, x_min] = box_color
            image[y_min:y_max, x_max] = box_color
            image[y_min, x_min:x_max] = box_color
            image[y_max, x_min:x_max] = box_color
        image = np.transpose(image, (2, 0, 1))  # HWC -> CHW
        writer.add_image(f'input/label_{image_index}', image, global_step=global_step)


def main():
    import argparse
    import os
    import shutil
    import sys
    import traceback

    import torch

    project_root = os.path.abspath(os.path.join(os.path.dirname(__file__), '..', '..'))
    sys.path.insert(0, project_root)

    from src.data import load_data
    from src.ssd import SSD
    from src.torch_utils.utils.logger import create_logger

    logger = create_logger('test_data', 'log', True)

    try:
        parser = argparse.ArgumentParser()
        parser.add_argument('dataset', help='Name of dataset to train')
        parser.add_argument('--class_list', help='Classes to train (otherwise all classes are used)')
        parser.add_argument('--limit', type=int, default=4, help='Max size fot the dataset')
        parser.add_argument('--size', help='Size of the data (will be resized and padded)')
        parser.add_argument('--path', help='Path to the dataset main folder')
        parser.add_argument('--output', nargs='?', default=os.path.join(project_root, 'output', 'test', 'data'),
                            help='Data directory where output files will be saved')
        arguments = parser.parse_args()

        class_list = arguments.class_list.split(',') if arguments.class_list else None
        data_size = [int(size) for size in arguments.size.split(',')] if arguments.size else None

        if os.path.exists(arguments.output):
            shutil.rmtree(arguments.output)
        os.makedirs(arguments.output, exist_ok=True)

        data, labels = load_data(
            arguments.dataset, arguments.path, False, class_list, arguments.limit,
            data_size, None, logger=logger)

        logger.info('-------- Starting Test Data --------')
        logger.info('From command : ' + ' '.join(sys.argv))
        logger.info('Data shape : {}'.format(data.shape))

        device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')

        from src.base.simple import BaseNetwork
        ssd_network = SSD(base_network=BaseNetwork(), input_sample=data).to(device)

        writer = SummaryWriter(log_dir=arguments.output, flush_secs=10)
        images = torch.as_tensor(data).to(device).float()
        writer.add_graph(ssd_network, images)

        test(data, labels, writer)
        writer.close()

    except Exception as error:
        logger.error(''.join(traceback.format_exception(*sys.exc_info())))
        raise error


if __name__ == '__main__':
    main()
