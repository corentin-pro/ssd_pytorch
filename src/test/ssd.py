import numpy as np
from torch.utils.tensorboard import SummaryWriter

from src.ssd import SSD


def anchor_summary(network: SSD, writer: SummaryWriter):
    # Tensorboard test : outputing anchors
    # depth first -> depth last
    images = np.zeros((len(network.layers), *network.base_input_shape[1:], network.base_input_shape[0]), dtype=np.uint8)
    for anchor in network.anchor_info:
        image = images[anchor.layer_index]
        max_height = image.shape[0] - 1
        max_width = image.shape[1] - 1
        box_color = network.box_colors[anchor.color_index]
        y_min = int(anchor.y_min * max_height)
        y_max = int(anchor.y_max * max_height)
        x_min = int(anchor.x_min * max_width)
        x_max = int(anchor.x_max * max_width)
        image[y_min, x_min:x_max] = box_color
        image[y_max, x_min:x_max] = box_color
        image[y_min:y_max, x_min] = box_color
        image[y_min:y_max, x_max] = box_color
    for layer_index, image in enumerate(images):
        writer.add_image(f'anchors/layer_{layer_index}', np.transpose(image, (2, 0, 1)))


def main():
    import argparse
    import os
    import shutil
    import sys
    import traceback

    import torch

    project_root = os.path.abspath(os.path.join(os.path.dirname(__file__), '..', '..'))
    sys.path.insert(0, project_root)

    from src.data import load_data
    from src.ssd import SSD
    from src.torch_utils.utils.logger import create_logger

    logger = create_logger('test_data', 'log', True)

    try:
        parser = argparse.ArgumentParser()
        parser.add_argument('dataset', help='Name of dataset to train')
        parser.add_argument('--class_list', help='Classes to train (otherwise all classes are used)')
        parser.add_argument('--limit', type=int, default=4, help='Max size fot the dataset')
        parser.add_argument('--size', help='Size of the data (will be resized and padded)')
        parser.add_argument('--path', help='Path to the dataset main folder')
        parser.add_argument('--output', nargs='?', default=os.path.join(project_root, 'output', 'test', 'ssd'),
                            help='Data directory where output files will be saved')
        arguments = parser.parse_args()

        class_list = arguments.class_list.split(',') if arguments.class_list else None
        data_size = [int(size) for size in arguments.size.split(',')] if arguments.size else None

        if os.path.exists(arguments.output):
            shutil.rmtree(arguments.output)
        os.makedirs(arguments.output, exist_ok=True)

        data, labels = load_data(
            arguments.dataset, arguments.path, class_list, arguments.limit,
            data_size, None, logger=logger)

        logger.info('-------- Starting Test SSD --------')
        logger.info('From command : ' + ' '.join(sys.argv))
        logger.info('Data shape : {}'.format(data.shape))

        device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')

        from src.base.simple import BaseNetwork
        ssd_network = SSD(base_network=BaseNetwork(), input_sample=data).to(device)

        print('\n --- Network ---')
        print(ssd_network)

        writer = SummaryWriter(log_dir=arguments.output, flush_secs=10)
        images = torch.as_tensor(data, dtype=torch.float32, device=device)
        writer.add_graph(ssd_network, images)

        anchor_summary(ssd_network, writer)
        writer.close()

    except Exception as error:
        logger.error(''.join(traceback.format_exception(*sys.exc_info())))
        raise error


if __name__ == '__main__':
    main()
