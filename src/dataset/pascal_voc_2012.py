import multiprocessing as mp
import os
from typing import List, Optional, Tuple
import xml.etree.ElementTree as ET

from PIL import Image, ImageDraw
import numpy as np

from src.data import resize_image
from src.torch_utils.utils.logger import DummyLogger


class PascalVoc2012():
    CLASSES = ['aeroplane', 'bicycle', 'bird', 'boat', 'bottle',
               'bus', 'car', 'cat', 'chair', 'cow',
               'diningtable', 'dog', 'horse', 'motorbike', 'person',
               'pottedplant', 'sheep', 'sofa', 'train', 'tvmonitor']
    DATASET_DIR = None
    ANNOTATION_DIR = None
    WORKER_ARGUMENTS = [[], None, None, None, None, None, None]
    IMAGE_DUMP = None

    @staticmethod
    def get_label_files(dataset_path: str) -> Tuple[str, List[str]]:
        annotation_dir = os.path.join(dataset_path, 'Annotations')
        return annotation_dir, os.listdir(annotation_dir)

    @staticmethod
    def load_data(
            dataset_path: str,
            return_images: bool = False,
            class_list: Optional[List[str]] = None,
            data_limit: Optional[int] = None,
            resize: Optional[Tuple[int, int]] = None,
            allow_truncated: bool = True,
            allow_occluded: bool = False,
            allow_difficult: bool = False,
            logger=DummyLogger()) -> Tuple[np.ndarray, np.ndarray]:
        if class_list is None:
            class_list = PascalVoc2012.CLASSES
        annotation_dir, label_files = PascalVoc2012.get_label_files(dataset_path)

        if data_limit is None:
            data_limit = len(label_files)
        pool_size = (mp.cpu_count() // 2) + 1 if data_limit > 100 else 1
        data_per_worker = data_limit // pool_size
        PascalVoc2012.DATASET_DIR = dataset_path
        PascalVoc2012.ANNOTATION_DIR = annotation_dir
        PascalVoc2012.WORKER_ARGUMENTS = [
            return_images, class_list, data_per_worker, resize,
            allow_truncated, allow_occluded, allow_difficult, logger]

        pool_arguments = []
        for pool_index in range(pool_size):
            pool_arguments.append(label_files[data_per_worker * pool_index:data_per_worker * (pool_index + 1)])

        logger.info('Loading PascalVoc2012 dataset...')
        pool = mp.Pool(processes=pool_size)
        data = pool.map(PascalVoc2012._data_worker, pool_arguments)
        pool.close()

        total_data = 0
        for _, label in data:
            total_data += len(label)
        if data_limit is not None and total_data < data_limit:
            PascalVoc2012.WORKER_ARGUMENTS = [
                return_images, class_list, data_limit - total_data, resize,
                allow_truncated, allow_occluded, allow_difficult, logger]
            data.append(PascalVoc2012._data_worker(label_files[data_per_worker * pool_size:]))

        max_label_count = max([label.shape[1] for _, label in data if len(label)])
        images = []
        labels = []
        for image, label in data:
            if len(label) == 0:
                continue
            elif label.shape[1] < max_label_count:
                label = np.concatenate(
                    [label,
                     np.zeros(
                         (len(label), max_label_count - label.shape[1], label.shape[2]),
                         dtype=label.dtype)],
                    axis=1)
            images.append(image)
            labels.append(label)
        images = np.concatenate(images)
        labels = np.concatenate(labels)
        logger.info('Dataset loaded : images {} ({}), labels {} ({})'.format(
            images.shape, images.dtype, labels.shape, labels.dtype))
        return images, labels

    @staticmethod
    def _data_worker(entry_list: list) -> Tuple[np.ndarray, np.ndarray]:
        (return_images, class_list, data_limit, resize, allow_truncated,
         allow_occluded, allow_difficult, logger) = PascalVoc2012.WORKER_ARGUMENTS
        images = []
        labels = []
        max_label_size = None

        for entry in entry_list:
            image_labels, image_name = PascalVoc2012.load_labels(
                entry, class_list=class_list, resize=resize,
                allow_truncated=allow_truncated, allow_occluded=allow_occluded, allow_difficult=allow_difficult,
                logger=logger)
            if image_labels is not None:
                if max_label_size is None:
                    max_label_size = len(image_labels)
                elif max_label_size > len(image_labels):
                    image_labels = np.concatenate(
                        [image_labels,
                         np.zeros(
                             (max_label_size - len(image_labels), image_labels.shape[1]),
                             dtype=image_labels.dtype)])
                elif max_label_size < len(image_labels):
                    for image_index, previous_labels in enumerate(labels):
                        labels[image_index] = np.concatenate(
                             [previous_labels,
                              np.zeros(
                                  (len(image_labels) - max_label_size, image_labels.shape[1]),
                                  dtype=previous_labels.dtype)])
                    max_label_size = len(image_labels)
                # Add image and labels to result list
                image, label = PascalVoc2012.resize_annotated_image(
                    os.path.join(PascalVoc2012.DATASET_DIR, 'JPEGImages', image_name),
                    image_labels,
                    return_image=return_images,
                    resize=resize)
                images.append(image)
                labels.append(label)
                if len(labels) >= data_limit:
                    break
        return np.asarray(images), np.asarray(labels)

    @staticmethod
    def load_labels(
            entry: str,
            class_list: Optional[List[str]] = None,
            resize: Optional[Tuple[int, int]] = None,
            allow_truncated: bool = True,
            allow_occluded: bool = False,
            allow_difficult: bool = False,
            logger=DummyLogger()) -> Tuple[Optional[np.ndarray], Optional[str]]:
        tree = ET.parse(os.path.join(PascalVoc2012.ANNOTATION_DIR, entry))
        root = tree.getroot()
        image_name = root.find('filename').text

        image_labels = []
        for object_element in root.iter('object'):
            object_class = object_element.find('name').text
            # Check object class
            if object_class in class_list:
                # Efficently parse object (avoid multiple find)
                elements = list(object_element)
                truncated = False
                occluded = False
                difficult = False
                object_bb = None
                for element in elements:
                    if element.tag == 'truncated':
                        truncated = element.text != '0'
                    if element.tag == 'occluded':
                        occluded = element.text != '0'
                    if element.tag == 'difficult':
                        difficult = element.text != '0'
                    if element.tag == 'bndbox':
                        object_bb = element
                # Check object is allowed
                if not allow_truncated and truncated:
                    continue
                if not allow_occluded and occluded:
                    continue
                if not allow_difficult and difficult:
                    continue

                # Add current object to entry_labels
                try:
                    y_min = object_bb.find('ymin').text
                    x_min = object_bb.find('xmin').text
                    y_max = object_bb.find('ymax').text
                    x_max = object_bb.find('xmax').text
                    y_min = int(y_min[:y_min.find('.')] if '.' in y_min else y_min) - 1
                    x_min = int(x_min[:x_min.find('.')] if '.' in x_min else x_min) - 1
                    y_max = int(y_min[:y_max.find('.')] if '.' in y_max else y_max) - 1
                    x_max = int(x_min[:x_max.find('.')] if '.' in x_max else x_max) - 1
                except ValueError as error:
                    logger.warning('Ignoring object {} of file {} :\n{}'.format(
                        object_class, root.find('filename').text, error))
                    continue
                image_labels.append(np.array([
                    (y_min + y_max) / 2,
                    (x_min + x_max) / 2,
                    y_max - y_min,
                    x_max - x_min,
                    class_list.index(object_class) + 1
                ], dtype=np.float32))
        if len(image_labels) == 0:
            return None, None
        return np.asarray(image_labels), image_name

    @staticmethod
    def resize_annotated_image(
            image_path: str,
            image_labels: np.ndarray,
            return_image: bool = False,
            resize: Optional[Tuple[int, int]] = None) -> Tuple[np.ndarray, np.ndarray]:
        dump_path = PascalVoc2012.IMAGE_DUMP
        labels = np.copy(image_labels)
        if resize is not None:
            image = Image.open(image_path)

            # Dumping original image
            if dump_path:
                test_image = image.copy()
                draw = ImageDraw.Draw(test_image)
                for label in labels:
                    if label[2] != 0:
                        draw.rectangle(
                            (label[0], label[1], label[0] + label[2], label[1] + label[3]),
                            outline=(255, 0, 0))
                test_image.save(os.path.join(dump_path, os.path.basename(image_path) + '_ori'), 'PNG')

            # Resizing image (keeping ratio)
            if image.size[0] != resize[0] or image.size[1] != resize[1]:
                resized_image = resize_image(image, resize)
                y_padding = (resize[1] - image.size[1]) // 2
                x_padding = (resize[0] - image.size[0]) // 2
                labels[:, 0] += y_padding
                labels[:, 1] += x_padding
            else:
                resized_image = image

            # Dumping annotated image
            if dump_path:
                test_image = resized_image.copy()
                draw = ImageDraw.Draw(test_image)
                for label in labels:
                    if label[2] != 0:
                        draw.rectangle(
                            (label[0], label[1], label[0] + label[2], label[1] + label[3]),
                            outline=(255, 0, 0))
                test_image.save(os.path.join(dump_path, os.path.basename(image_path)), 'PNG')

            resized_image = np.asarray(resized_image)
        else:
            resized_image = np.asarray(Image.open(image_path))
        labels[:, 0] /= resized_image.shape[1]
        labels[:, 1] /= resized_image.shape[0]
        labels[:, 2] /= resized_image.shape[1]
        labels[:, 3] /= resized_image.shape[0]
        return resized_image if return_image else image_path, labels
