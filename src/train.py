from typing import List, Tuple

import numpy as np
import torch
import torch.nn as nn
from torch.utils.tensorboard import SummaryWriter

from config.ssd_config import SSDConfig
from config.train_config import TrainConfig
from src.box import get_boxes, fast_nms
from src.ssd import SSD
from src.test.data import test as data_summary
from src.test.ssd import anchor_summary
from src.torch_utils.trainer import Trainer
from src.torch_utils.utils.logger import DummyLogger
from src.torch_utils.utils.batch_generator import BatchGenerator


class PreProcess(nn.Module):
    def __init__(self, device: str):
        super().__init__()
        self.device = device

    def forward(self, input_data: torch.Tensor) -> torch.Tensor:
        return input_data / 255.


class AugmentPreProcess(nn.Module):
    def __init__(self, image_shape: Tuple[int, int, int], device: str):
        super().__init__()
        self.image_shape = image_shape
        self.device = device

        self.offset_tensor = torch.empty(image_shape, dtype=torch.float32, device=device)
        self.scale_tensor = torch.empty(1, dtype=torch.float32, device=device)

    def forward(self, input_data: torch.Tensor) -> torch.Tensor:
        return (((input_data / 255.0) + self.offset_tensor.uniform_(-0.1, 0.1))
                * self.scale_tensor.uniform_(0.8, 1.2)).clamp(0.0, 1.0)


class JacardOverlap(nn.Module):
    def __init__(self):
        super().__init__()

    def forward(self, anchors: torch.Tensor, labels: torch.Tensor) -> torch.Tensor:
        """
        Assuming rank 2 (number of boxes, locations), location is (y, x, h, w)
        Jaccard overlap : A ∩ B / A ∪ B = A ∩ B / (area(A) + area(B) - A ∩ B)
        Return:
            jaccard overlap: (tensor) Shape: [predictions.size(0), labels.size(0)]
        """
        anchors_count = anchors.size(0)
        labels_count = labels.size(0)

        # Getting coords (y_min, x_min, y_max, x_max) repeated to fill (anchor count, label count)
        anchor_coords = torch.cat([
            anchors[:, :2] - (anchors[:, 2:] / 2),
            anchors[:, :2] + (anchors[:, 2:] / 2)], 1).unsqueeze(1).expand(anchors_count, labels_count, 4)
        label_coords = torch.cat([
            labels[:, :2] - (labels[:, 2:] / 2),
            labels[:, :2] + (labels[:, 2:] / 2)], 1).unsqueeze(0).expand(anchors_count, labels_count, 4)

        mins = torch.max(anchor_coords, label_coords)[:, :, :2]
        maxes = torch.min(anchor_coords, label_coords)[:, :, 2:]

        inter_coords = torch.clamp(maxes - mins, min=0)
        inter_area = inter_coords[:, :, 0] * inter_coords[:, :, 1]

        anchor_areas = (anchors[:, 2] * anchors[:, 3]).unsqueeze(1).expand_as(inter_area)
        label_areas = (labels[:, 2] * labels[:, 3]).unsqueeze(0).expand_as(inter_area)

        union_area = anchor_areas + label_areas - inter_area
        return inter_area / union_area


def draw_boxes(image: np.ndarray, boxes: List[np.ndarray], box_color: np.ndarray):
    """
    images are in format [NWHC]
    box : y, x, h, w
    """
    for box in boxes:
        if box[3] == 0:
            return
        center_y = box[0] * image.shape[0]
        center_x = box[1] * image.shape[1]
        halh_height = (box[2] * image.shape[0]) / 2
        halh_width = (box[3] * image.shape[1]) / 2
        y_min = int(round(center_y - halh_height))
        y_max = int(round(center_y + halh_height))
        x_min = int(round(center_x - halh_width))
        x_max = int(round(center_x + halh_width))
        if y_max >= image.shape[0]:
            y_max = image.shape[0] - 1
        if x_max >= image.shape[1]:
            x_max = image.shape[1] - 1
        if y_min > y_max:
            y_min = y_max
        if x_min > x_max:
            x_min = x_max
        image[y_min:y_max, x_min] = box_color
        image[y_min:y_max, x_max] = box_color
        image[y_min, x_min:x_max] = box_color
        image[y_max, x_min:x_max] = box_color


class SSDLoss(nn.Module):
    def __init__(self, anchors: torch.Tensor, label_per_image: int, negative_mining_ratio: int):
        super().__init__()
        self.anchors = anchors
        self.anchor_count = anchors.size(0)
        self.label_per_image = label_per_image
        self.negative_mining_ratio = negative_mining_ratio

        self.overlap = JacardOverlap()
        self.matches = []
        # self.negative_matches = []
        self.positive_class_loss = torch.Tensor()
        self.negative_class_loss = torch.Tensor()
        self.localization_loss = torch.Tensor()
        self.class_loss = torch.Tensor()
        self.final_loss = torch.Tensor()

    def forward(self, input_data: torch.Tensor, input_labels: torch.Tensor) -> torch.Tensor:
        batch_size = input_data.size(0)
        expanded_anchors = self.anchors[:, :4].unsqueeze(0).unsqueeze(2).expand(
            batch_size, self.anchor_count, self.label_per_image, 4)
        expanded_labels = input_labels[:, :, :SSDConfig.LOCATION_DIMMENSION].unsqueeze(1).expand(
            batch_size, self.anchor_count, self.label_per_image, SSDConfig.LOCATION_DIMMENSION)
        objective_pos = (expanded_labels[:, :, :, :2] - expanded_anchors[:, :, :, :2]) / (
            expanded_anchors[:, :, :, 2:])
        objective_size = torch.log(expanded_labels[:, :, :, 2:] / expanded_anchors[:, :, :, 2:])

        positive_objectives = []
        positive_predictions = []
        positive_class_loss = []
        negative_class_loss = []
        self.matches = []
        # self.negative_matches = []
        for batch_index in range(batch_size):
            predictions = input_data[batch_index]
            labels = input_labels[batch_index]
            overlaps = self.overlap(self.anchors[:, :4], labels[:, :4])
            mask = (overlaps >= TrainConfig.LABEL_MATCH_IOU).long()
            match_indices = torch.nonzero(mask, as_tuple=False)
            self.matches.append(match_indices.detach().cpu())

            mining_count = self.negative_mining_ratio * len(self.matches[-1])
            masked_prediction = predictions[:, SSDConfig.LOCATION_DIMMENSION] + torch.max(mask, dim=1)[0]
            non_match_indices = torch.argsort(masked_prediction, dim=-1, descending=False)[:mining_count]
            # self.negative_matches.append(non_match_indices.detach().cpu())

            for anchor_index, label_index in match_indices:
                positive_predictions.append(predictions[anchor_index])
                positive_objectives.append(
                    torch.cat((
                        objective_pos[batch_index, anchor_index, label_index],
                        objective_size[batch_index, anchor_index, label_index]), dim=-1))
                positive_class_loss.append(torch.log(
                    predictions[anchor_index, SSDConfig.LOCATION_DIMMENSION + labels[label_index, -1].long()]))

            for anchor_index in non_match_indices:
                negative_class_loss.append(
                    torch.log(predictions[anchor_index, SSDConfig.LOCATION_DIMMENSION]))

        positive_predictions = torch.stack(positive_predictions)
        positive_objectives = torch.stack(positive_objectives)
        self.positive_class_loss = -torch.sum(torch.stack(positive_class_loss))
        self.negative_class_loss = -torch.sum(torch.stack(negative_class_loss))
        self.localization_loss = nn.functional.smooth_l1_loss(
            positive_predictions[:, :SSDConfig.LOCATION_DIMMENSION],
            positive_objectives)
        self.class_loss = self.positive_class_loss + self.negative_class_loss
        self.final_loss = (TrainConfig.LOCALIZATION_LOSS_WEIGHT * self.localization_loss) + self.class_loss
        return self.final_loss


class Train(nn.Module):
    '''Trainer class, mainly for graph summary'''
    def __init__(self, network: SSD, loss: nn.Module):
        super().__init__()
        self.network = network
        self.loss = loss

    def forward(self, input_data: torch.Tensor, input_labels: torch.Tensor, batch_size: int = 0) -> Tuple[
            torch.Tensor, torch.Tensor, torch.Tensor, torch.Tensor]:
        if batch_size == 0:
            batch_size = input_data.size(0)
        outputs = self.network(input_data)
        localization_loss, class_loss, final_loss = self.loss(
            outputs, self.network.anchors, input_labels, batch_size)
        return localization_loss, class_loss, final_loss


class CustomTrainer(Trainer):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.network: SSD
        self.criterion: SSDLoss

        self.running_loc_loss = 0.0
        self.running_class_loss = 0.0
        self.running_class_pos_loss = 0.0
        self.train_matches = []

        self.val_loc_loss = 0.0
        self.val_class_loss = 0.0
        self.val_class_pos_loss = 0.0

    def train_step_callback(
            self,
            _batch_inputs: torch.Tensor, _processed_inputs: torch.Tensor,
            _batch_labels: torch.Tensor, _network_outputs: torch.Tensor,
            _loss: float, _accuracy: float):
        self.running_loc_loss += self.criterion.localization_loss.item()
        self.running_class_loss += self.criterion.class_loss.item()
        self.running_class_pos_loss += self.criterion.positive_class_loss.item()

    def val_step_callback(
            self,
            _batch_inputs: torch.Tensor, _processed_inputs: torch.Tensor,
            _batch_labels: torch.Tensor, _network_outputs: torch.Tensor,
            _loss: float, _accuracy: float):
        self.val_loc_loss += self.criterion.localization_loss.item()
        self.val_class_loss += self.criterion.class_loss.item()
        self.val_class_pos_loss += self.criterion.positive_class_loss.item()

    def pre_summary_callback(self):
        self.train_matches = self.criterion.matches

    def summary_callback(
            self,
            _train_inputs: torch.Tensor, _train_processed: torch.Tensor,
            _train_labels: torch.Tensor, _train_outputs: torch.Tensor, train_running_count: int,
            _val_inputs: torch.Tensor, _val_processed: torch.Tensor,
            _val_labels: torch.Tensor, _val_outputs: torch.Tensor, val_running_count: int):
        self.writer_train.add_scalar(
            'loss/localization', self.running_loc_loss / train_running_count,
            global_step=self.batch_generator_train.global_step)
        self.writer_train.add_scalar(
            'loss/class', self.running_class_loss / train_running_count,
            global_step=self.batch_generator_train.global_step)
        self.writer_train.add_scalar(
            'loss/class_pos', self.running_class_pos_loss / train_running_count,
            global_step=self.batch_generator_train.global_step)

        self.writer_val.add_scalar(
            'loss/localization', self.val_loc_loss / val_running_count,
            global_step=self.batch_generator_train.global_step)
        self.writer_val.add_scalar(
            'loss/class', self.val_class_loss / val_running_count,
            global_step=self.batch_generator_train.global_step)
        self.writer_val.add_scalar(
            'loss/class_pos', self.val_class_pos_loss / val_running_count,
            global_step=self.batch_generator_train.global_step)

        self.running_loc_loss = 0.0
        self.running_class_loss = 0.0
        self.running_class_pos_loss = 0.0
        self.val_loc_loss = 0.0
        self.val_class_loss = 0.0
        self.val_class_pos_loss = 0.0

    def image_callback(
            self,
            _train_inputs: torch.Tensor, _train_processed: torch.Tensor,
            _train_labels: torch.Tensor, train_outputs: torch.Tensor,
            val_inputs: torch.Tensor, _val_processed: torch.Tensor,
            _val_labels: torch.Tensor, val_outputs: torch.Tensor):

        self.save_images(
            (self.processed_inputs.detach() * 255.0).type(torch.uint8).cpu().numpy(),
            train_outputs.detach().cpu().numpy(), self.batch_generator_train,
            self.train_matches, self.writer_train, self.batch_generator_train.global_step)
        self.save_images(
            (self.pre_process(val_inputs) * 255.0).type(torch.uint8).detach().cpu().numpy(),
            val_outputs.detach().cpu().numpy(), self.batch_generator_val,
            self.criterion.matches, self.writer_val, self.batch_generator_train.global_step)

    def save_images(
            self,
            batch_inputs: np.ndarray, predictions: np.ndarray, batch_generator: BatchGenerator,
            matches: List[torch.Tensor], writer: SummaryWriter, global_step: int):
        box_color = np.asarray([255, 0, 0], dtype=np.uint8)
        image_count = min(len(batch_inputs), TrainConfig.IMAGE_MAX_OUTPUT)

        data_summary(batch_generator.batch_data[:image_count], batch_generator.batch_label[:image_count],
                     writer, global_step)
        for batch_index in range(image_count):
            image = np.copy(batch_inputs[batch_index])
            image_size = image.shape[1:]
            image = np.transpose(image, (1, 2, 0))  # CHW -> HWC

            # Match summary
            if TrainConfig.IMAGE_MATCHES:
                match_image = np.copy(image)
                for anchor_index, _ in matches[batch_index]:
                    anchor = self.network.anchor_info[anchor_index]
                    anchor_color = self.network.box_colors[anchor.color_index]
                    y_min = int(anchor.y_min * (image_size[0] - 1))
                    y_max = int(anchor.y_max * (image_size[0] - 1))
                    x_min = int(anchor.x_min * (image_size[1] - 1))
                    x_max = int(anchor.x_max * (image_size[1] - 1))
                    match_image[y_min, x_min:x_max] = anchor_color
                    match_image[y_max, x_min:x_max] = anchor_color
                    match_image[y_min:y_max, x_min] = anchor_color
                    match_image[y_min:y_max, x_max] = anchor_color
                match_image = np.transpose(match_image, (2, 0, 1))  # depth last -> depth first
                writer.add_image(f'matches/image_{batch_index}', match_image, global_step=global_step)

            # FP summary
            # fp_image = np.copy(image)
            # for anchor_index in ssd_loss.negative_matches[batch_index]:
            #     anchor = ssd_network.anchor_info[anchor_index]
            #     box_color = ssd_network.box_colors[anchor.color_index]
            #     y_min = int(anchor.y_min * (image_size[0] - 1))
            #     y_max = int(anchor.y_max * (image_size[0] - 1))
            #     x_min = int(anchor.x_min * (image_size[1] - 1))
            #     x_max = int(anchor.x_max * (image_size[1] - 1))
            #     fp_image[y_min, x_min:x_max] = box_color
            #     fp_image[y_max, x_min:x_max] = box_color
            #     fp_image[y_min:y_max, x_min] = box_color
            #     fp_image[y_min:y_max, x_max] = box_color
            # fp_image = np.transpose(fp_image, (2, 0, 1))  # depth last -> depth first
            # writer.add_image(f'negative_matches/image_{batch_index}', fp_image, global_step=global_step)

            if TrainConfig.IMAGE_ALL_OUTPUTS:
                all_image = np.copy(image)
            if TrainConfig.IMAGE_PRE_NMS:
                pre_nms_image = np.copy(image)
            final_image = np.copy(image)
            for class_index in range(self.network.classes - 1):
                current_index = class_index + SSDConfig.LOCATION_DIMMENSION + 1
                all_boxes = get_boxes(
                    predictions[batch_index], self.network.anchors_numpy, current_index)
                if TrainConfig.IMAGE_ALL_OUTPUTS:
                    draw_boxes(all_image, all_boxes, box_color)
                filtered_boxes = np.asarray(
                    [box for box in all_boxes if box[-1] > SSDConfig.CONFIDENCE_THRESHOLD])
                if len(filtered_boxes) == 0:
                    continue
                if TrainConfig.IMAGE_PRE_NMS:
                    draw_boxes(pre_nms_image, filtered_boxes, box_color)
                post_nms_boxes = fast_nms(filtered_boxes, SSDConfig.NMS_IOU)
                draw_boxes(final_image, post_nms_boxes, box_color)
            if TrainConfig.IMAGE_ALL_OUTPUTS:
                all_image = np.transpose(all_image, (2, 0, 1))  # HWC -> CHW
                writer.add_image(f'output/all_{batch_index}', all_image, global_step=global_step)
            if TrainConfig.IMAGE_PRE_NMS:
                pre_nms_image = np.transpose(pre_nms_image, (2, 0, 1))  # HWC -> CHW
                writer.add_image(f'output/pre_nms_{batch_index}', pre_nms_image, global_step=global_step)
            final_image = np.transpose(final_image, (2, 0, 1))  # HWC -> CHW
            writer.add_image(f'output/final_{batch_index}', final_image, global_step=global_step)


def train(batch_generator_train: BatchGenerator, batch_generator_val: BatchGenerator,
          output_dir: str, logger=DummyLogger):
    """
    images are in format [NCHW]
    label are sequences of boxes (box : x, y, w, h, one hot for class)
    """
    device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')

    from src.base.simple import BaseNetwork
    ssd_network = SSD(base_network=BaseNetwork(), input_sample=batch_generator_train.batch_data).to(device)
    ssd_loss = SSDLoss(
        ssd_network.anchors, batch_generator_train.label.shape[1], TrainConfig.HARD_NEGATIVE_MINING_RATIO)

    trainer = CustomTrainer(
        batch_generator_train, batch_generator_val,
        device, output_dir,
        ssd_network,
        PreProcess(device).to(device),
        torch.optim.Adam(ssd_network.parameters(), lr=TrainConfig.LEARNING_RATE, weight_decay=TrainConfig.WEIGHT_DECAY),
        ssd_loss,
        TrainConfig.SKIP_FIRST_EPOCHS, TrainConfig.SUMMARIES_PER_EPOCH, TrainConfig.IMAGES_PER_EPOCH,
        logger=logger)
    if TrainConfig.IMAGE_ANCHORS:
        anchor_summary(ssd_network, trainer.writer_train)
    trainer.fit(TrainConfig.TRAIN_EPOCH)
