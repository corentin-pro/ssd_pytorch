import argparse
import os
import shutil
import sys
import traceback

from PIL import Image
import numpy as np

from config.train_config import TrainConfig
from src.data import load_data, resize_image
from src.torch_utils.utils.batch_generator import BatchGenerator
from src.torch_utils.utils.logger import create_logger


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('dataset', help='Name of dataset to train')
    parser.add_argument('--class-list', nargs='*', help='Classes to train (otherwise all classes are used)')
    parser.add_argument('--limit', type=int, help='Max size fot the dataset')
    parser.add_argument('--size', nargs=2, type=int, help='Size of the data (will be resized and padded)')
    parser.add_argument('--path', help='Path to the dataset main folder')
    parser.add_argument('--save', help='Path to the folder containing the save files (avoid reloading data)')
    parser.add_argument('--preload', action='store_true', default=False, help='Cache data in RAM')
    parser.add_argument('--split', type=float, default=0.05, help='Validation split ratio')
    parser.add_argument('--output', nargs='?', default=os.path.join('output', 'ssd'),
                        help='Data directory where output files will be saved')
    arguments = parser.parse_args()

    class_list = arguments.class_list if arguments.class_list else None
    save_name = os.path.join(arguments.save, '{}'.format(arguments.dataset)) if arguments.save else None
    if save_name:
        if class_list:
            save_name += '_class_' + '_'.join(class_list)
        if arguments.size:
            save_name += '_size_' + '_'.join([str(size) for size in arguments.size])
        if arguments.limit:
            save_name += '_limit_{}'.format(arguments.limit)

    if os.path.exists(arguments.output):
        shutil.rmtree(arguments.output)
    os.makedirs(arguments.output, exist_ok=True)

    try:
        logger = create_logger('train_ssd', os.path.join(arguments.output, 'log'), True)

        train_data, train_labels = load_data(
            arguments.dataset, arguments.path, return_images=arguments.preload, class_list=class_list,
            limit=arguments.limit, size=arguments.size, save=save_name, logger=logger)

        # Data split
        indices = np.arange(len(train_data))
        np.random.shuffle(indices)
        validation_count = int(len(train_data) * arguments.split)
        val_data = train_data[indices[-validation_count:]]
        val_labels = train_labels[indices[-validation_count:]]
        train_data = train_data[indices[:-validation_count]]
        train_labels = train_labels[indices[:-validation_count]]

        def data_processor(image_path: str) -> np.ndarray:
            nonlocal arguments
            image = Image.open(image_path)
            if arguments.size and image.size[0] != arguments.size[0] or image.size[1] != arguments.size[1]:
                image = resize_image(image, arguments.size)
            return np.transpose(np.asarray(image), (2, 0, 1))

        with BatchGenerator(
                train_data, train_labels, TrainConfig.BATCH_SIZE,
                data_processor=data_processor if not arguments.preload else None,
                preload=arguments.preload, shuffle=True) as batch_generator_train:
            with BatchGenerator(
                    val_data, val_labels, TrainConfig.BATCH_SIZE,
                    data_processor=data_processor if not arguments.preload else None,
                    preload=arguments.preload, shuffle=False) as batch_generator_val:

                logger.info('-------- Starting train --------')
                logger.info('From command : ' + ' '.join(sys.argv))
                logger.info('Data shape : {}, label shape : {}'.format(
                    batch_generator_train.data.shape, batch_generator_train.label.shape))
                logger.info('Val data shape : {}, val label shape : {}'.format(
                    batch_generator_val.data.shape, batch_generator_val.label.shape))
                logger.info('Steps per epoch : {}, max epoch : {}'.format(
                    batch_generator_train.step_per_epoch, TrainConfig.TRAIN_EPOCH))

                from src.train import train
                train(batch_generator_train, batch_generator_val, output_dir=arguments.output, logger=logger)
    except Exception as error:
        logger.error(''.join(traceback.format_exception(*sys.exc_info())))
        raise error


if __name__ == '__main__':
    main()
