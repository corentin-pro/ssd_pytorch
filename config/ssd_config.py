class SSDConfig(type):
    LAYER_FEATURES = [
        128,
        128,
        128,
        64,
        0
    ]
    LAYER_ARGS = [
        {'stride': 2},
        {},
        {},
        {'batch_norm': False},
        {'disable': True}
    ]
    LAYER_RATIOS = [
        [1.0, 2.0, 0.5],
        [1.0, 2.0, 3.0, 0.5, 1.0/3.0, 1.0, 2.0, 3.0, 0.5, 1.0/3.0],
        [1.0, 2.0, 3.0, 0.5, 1.0/3.0, 1.0, 2.0, 3.0, 0.5, 1.0/3.0],
        [1.0, 2.0, 3.0, 0.5, 1.0/3.0, 1.0, 2.0, 3.0, 0.5, 1.0/3.0],
        [1.0, 2.0, 3.0, 0.5, 1.0/3.0, 1.0, 2.0, 3.0, 0.5, 1.0/3.0],
    ]
    BOX_SIZE_FACTORS = [1.5, 1.5, 1.5, 1.5, 1.5, 0.7, 0.7, 0.7, 0.7, 0.7]

    LOCATION_DIMMENSION = 4
    OUTPUT_CLASSES = 20

    CONFIDENCE_THRESHOLD = 0.5
    NMS_IOU = 0.3
