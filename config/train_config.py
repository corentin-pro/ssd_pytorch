class TrainConfig(type):
    BATCH_SIZE = 12
    TRAIN_EPOCH = 50

    SUMMARIES_PER_EPOCH = 8
    IMAGES_PER_EPOCH = 2
    IMAGE_MAX_OUTPUT = 2
    SKIP_FIRST_EPOCHS = 0

    NETWORK_SUMMARIES = True

    IMAGE_ANCHORS = True
    IMAGE_MATCHES = True
    IMAGE_ALL_OUTPUTS = False
    IMAGE_PRE_NMS = False

    MAX_CHECKPOINT = 3

    LEARNING_RATE = 1e-4
    WEIGHT_DECAY = 1e-5
    LOCALIZATION_LOSS_WEIGHT = 1

    LABEL_MATCH_IOU = 0.3  # Minimal IoU to match a label with an anchor
    HARD_NEGATIVE_MINING_RATIO = 2  # Ratio of negative box to use compared to positives (otherwise to many negative)
